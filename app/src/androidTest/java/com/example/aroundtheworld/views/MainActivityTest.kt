package com.example.aroundtheworld.views

import android.content.res.Resources
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.example.aroundtheworld.EspressoIdlingResource
import com.example.aroundtheworld.R
import com.example.aroundtheworld.adapters.CountryViewHolder
import com.example.aroundtheworld.viewmodels.MainActivityViewModel
import org.hamcrest.CoreMatchers.not
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_isActivityDisplayed() {
        onView(withId(R.id.activity_main)).check(matches(isDisplayed()))
    }

    @Test
    fun test_isToastDisplayed_For_Scenario_CountryNotFound() {
        val nonExistingCountry = "aaaaa"

        onView(withId(R.id.et_searchbar)).perform(typeText(nonExistingCountry))
        onView(withId(R.id.btn_search)).perform(click())

        onView(withText(MainActivityViewModel.TOASTVALUE_UNMATCHEDCOUNTRYNAME)).inRoot(
            ToastMatcher()
        ).check(matches(isDisplayed()))
    }

    @Test
    fun test_isToastDisplayed_For_Scenario_EmptyString() {
        val emptyString = String()

        onView(withId(R.id.et_searchbar)).perform(typeText(emptyString))
        onView(withId(R.id.btn_search)).perform(click())

        onView(withText(MainActivityViewModel.TOASTVALUE_EMPTYSTRING)).inRoot(ToastMatcher()).check(
            matches(
                isDisplayed()
            )
        )
    }

//    for successful test device must not be connected to the internet
    @Test @Ignore
    fun test_isToastDisplayed_For_Scenario_NoInternetConnection() {
        val countryName = "croatia"

        onView(withId(R.id.et_searchbar)).perform(typeText(countryName))
        onView(withId(R.id.btn_search)).perform(click())

        onView(withText(MainActivityViewModel.TOASTVALUE_ERRORMESSAGE)).inRoot(ToastMatcher()).check(matches(isDisplayed()))
    }

    @Test
    fun test_is_et_searchbar_empty_after_clickOnBtnSearch() {
        val existingCountry = "croatia"

        onView(withId(R.id.et_searchbar)).perform(typeText(existingCountry))
        onView(withId(R.id.btn_search)).perform(click())

        onView(withId(R.id.et_searchbar)).check(matches(withText(String())))
    }

    @Test
    fun test_isExpandableViewDisplayed_For_Scenario_ClickOnHiddenExpandableView() {
        val existingCountry = "croatia"
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)

        onView(withId(R.id.et_searchbar)).perform(typeText(existingCountry))
        onView(withId(R.id.btn_search)).perform(click())
        onView(withId(R.id.rv_countries)).perform(
            actionOnItemAtPosition<CountryViewHolder>(
                0,
                click()
            )
        )

        onView(withId(R.id.expandableLayout)).check(matches(isDisplayed()))
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Test
    fun test_isExpandableViewHidden_For_Scenario_ClickOnDisplayedExpandableView() {
        val existingCountry = "croatia"
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)

        onView(withId(R.id.et_searchbar)).perform(typeText(existingCountry))
        onView(withId(R.id.btn_search)).perform(click())
        onView(withId(R.id.rv_countries)).perform(
            actionOnItemAtPosition<CountryViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.tv_country_name)).perform(click())

        onView(withId(R.id.expandableLayout)).check(matches(not(isDisplayed())))
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }
}