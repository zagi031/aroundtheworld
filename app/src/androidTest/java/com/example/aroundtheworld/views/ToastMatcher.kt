@file:Suppress("DEPRECATION")

package com.example.aroundtheworld.views

import android.os.IBinder
import android.view.WindowManager
import androidx.test.espresso.Root
import org.junit.internal.matchers.TypeSafeMatcher
import org.junit.runner.Description

@Suppress("DEPRECATION")
class ToastMatcher : TypeSafeMatcher<Root?>() {

    override fun describeTo(description: org.hamcrest.Description?) {
        description?.appendText("is toast")
    }

    override fun matchesSafely(item: Root?): Boolean {
        val type: Int? = item?.windowLayoutParams?.get()?.type
        if (type == WindowManager.LayoutParams.TYPE_TOAST) {
            val windowToken: IBinder = item.decorView.windowToken
            val appToken: IBinder = item.decorView.applicationWindowToken
            if (windowToken === appToken) { // means this window isn't contained by any other windows.
                return true
            }
        }
        return false
    }

}