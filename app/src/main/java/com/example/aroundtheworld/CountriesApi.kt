package com.example.aroundtheworld

import com.example.aroundtheworld.models.Country
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CountriesApi {
    @GET("all?fields=name;capital;nativeName;region;subregion;population;area;flag;altSpellings")
    fun getAllCountries(): Call<List<Country>>

    @GET("name/{name}?fields=name;capital;nativeName;region;subregion;population;area;flag;altSpellings")
    fun getCountriesByName(@Path("name") name: String): Call<List<Country>>
}