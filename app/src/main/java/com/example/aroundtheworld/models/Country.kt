package com.example.aroundtheworld.models

import com.google.gson.annotations.SerializedName

// name, native name, capital, region, subregion, population, area,  flag
data class Country(
        val name: String,
        val nativeName: String,
        val capital: String,
        val region: String,
        val subregion: String,
        val population: Int,
        val area: Float,
        val altSpellings: List<String>,
        @SerializedName("flag") val flagURL: String){
        var isExpanded:Boolean = false
}