package com.example.aroundtheworld.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.aroundtheworld.CountriesApi
import com.example.aroundtheworld.EspressoIdlingResource
import com.example.aroundtheworld.R
import com.example.aroundtheworld.models.Country
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    companion object {
        const val BASE_URL = "https://restcountries.eu/rest/v2/"
        const val TOASTVALUE_EMPTYSTRING = "Please enter country name"
        const val TOASTVALUE_UNMATCHEDCOUNTRYNAME = "Country not found"
        const val TOASTVALUE_ERRORMESSAGE = "Some error occurred, please check your internet connection"
    }

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast
    private val _countries = MutableLiveData<List<Country>>()
    val countries: LiveData<List<Country>>
        get() = _countries
    private val retrofit =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build()
    private val countriesApi = retrofit.create(CountriesApi::class.java)

    fun getCountries(name: String) {
        when {
            name.isEmpty() -> {
                _toast.value = TOASTVALUE_EMPTYSTRING
            }
            name.toLowerCase(Locale.ROOT) == "all" -> {
                getAllCountries()
            }
            else -> {
                viewModelScope.launch(Dispatchers.IO) {
    //                line added because of espresso testing
                    EspressoIdlingResource.increment()

                    val call = countriesApi.getCountriesByName(name)
                    call.enqueue(object : Callback<List<Country>> {
                        override fun onResponse(
                            call: Call<List<Country>>,
                            response: Response<List<Country>>
                        ) {
                            if (!response.isSuccessful) {
                                if (response.code() == 404) {
                                    _toast.value = TOASTVALUE_UNMATCHEDCOUNTRYNAME
                                    //line added because of espresso testing
                                    EspressoIdlingResource.decrement()
                                } else {
                                    _toast.value = "Code: " + response.code().toString()
                                    //line added because of espresso testing
                                    EspressoIdlingResource.decrement()
                                }
                                return
                            }
                            _countries.value = response.body()
                            //line added because of espresso testing
                            EspressoIdlingResource.decrement()
                        }

                        override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                            _toast.value = TOASTVALUE_ERRORMESSAGE
                            //line added because of espresso testing
                            EspressoIdlingResource.decrement()
                        }
                    })
                }
            }
        }
    }

    private fun getAllCountries() {
        viewModelScope.launch(context = Dispatchers.IO) {
            val call = countriesApi.getAllCountries()
            call.enqueue(object : Callback<List<Country>> {
                override fun onResponse(
                    call: Call<List<Country>>,
                    response: Response<List<Country>>
                ) {
                    if (!response.isSuccessful) {
                        _toast.value = "Code: " + response.code().toString()
                        return
                    }
                    _countries.value = response.body()
                }

                override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                    _toast.value = t.message
                }
            })
        }
    }
}