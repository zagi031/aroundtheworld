package com.example.aroundtheworld.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.caverock.androidsvg.SVG
import com.caverock.androidsvg.SVGParseException
import com.example.aroundtheworld.R
import com.example.aroundtheworld.models.Country
import kotlinx.android.synthetic.main.country_rv_item.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.HttpURLConnection
import java.net.URL

class CountriesAdapter(private val context: Context) : RecyclerView.Adapter<CountryViewHolder>() {
    private val countries = mutableListOf<Country>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.country_rv_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(countries[position])
        holder.itemView.progressBar.visibility = View.VISIBLE
        holder.itemView.tv_country_name.setOnClickListener {
            countries[position].isExpanded = !countries[position].isExpanded
            notifyItemChanged(position)
        }
        GlobalScope.launch(context = Dispatchers.IO) {
            try {
                val connection =
                    URL(countries[position].flagURL).openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                val inputStream = connection.inputStream
                val svg: SVG = SVG.getFromInputStream(inputStream)
                connection.disconnect()

                withContext(Dispatchers.Main) {
                    holder.itemView.progressBar.visibility = View.GONE
                    holder.itemView.iv_country_flag.setSVG(svg)
                }
            } catch (e: SVGParseException) {
                val drawable = context.getDrawable(R.drawable.ic_error)
                withContext(Dispatchers.Main) {
                    holder.itemView.progressBar.visibility = View.GONE
                    holder.itemView.iv_country_flag.setImageDrawable(drawable)
                }
            }
        }
    }

    override fun getItemCount() = countries.size

    fun setData(countries: List<Country>) {
        this.countries.clear()
        this.countries.addAll(countries)
        notifyDataSetChanged()
    }

    fun deleteData() {
        this.countries.clear()
        notifyDataSetChanged()
    }
}