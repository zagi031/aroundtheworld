package com.example.aroundtheworld.adapters

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.text.Html
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.aroundtheworld.R
import com.example.aroundtheworld.models.Country
import kotlinx.android.synthetic.main.country_rv_item.*
import kotlinx.android.synthetic.main.country_rv_item.view.*
import java.io.InputStream
import java.net.URL

@Suppress("DEPRECATION")
class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    @SuppressLint("SetTextI18n")
    fun bind(country: Country) {
        var srcString = "<b>${country.name}</b><br>(${country.nativeName})"
        itemView.tv_country_name.text = Html.fromHtml(srcString)
        srcString = "<b>Area</b>: ${country.area} m²"
        itemView.tv_country_area.text = Html.fromHtml(srcString)
        srcString = "<b>Capital city</b>: ${country.capital}"
        itemView.tv_country_capital.text = Html.fromHtml(srcString)
        srcString = "<b>Population</b>: ${country.population}"
        itemView.tv_country_population.text = Html.fromHtml(srcString)
        srcString = "<b>Region</b>: ${country.region}, ${country.subregion}"
        itemView.tv_country_region.text = Html.fromHtml(srcString)

        if(country.isExpanded){
            itemView.expandableLayout.visibility = View.VISIBLE
        }
        else itemView.expandableLayout.visibility = View.GONE
    }
}