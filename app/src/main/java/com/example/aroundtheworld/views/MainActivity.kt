package com.example.aroundtheworld.views

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.aroundtheworld.R
import com.example.aroundtheworld.adapters.CountriesAdapter
import com.example.aroundtheworld.models.Country
import com.example.aroundtheworld.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var countriesAdapter: CountriesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecycler()
        val viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        btn_search.setOnClickListener {
            countriesAdapter.deleteData()
            progressBar.visibility = View.VISIBLE
            viewModel.getCountries(et_searchbar.text.toString())
            resetEditTextView()
        }

        viewModel.toast.observe(this, Observer {
            progressBar.visibility = View.GONE
            displayToast(it)
        })
        viewModel.countries.observe(this, Observer {
            progressBar.visibility = View.GONE
            displayCountries(it)
        })
    }

    private fun setupRecycler() {
        rv_countries.layoutManager = LinearLayoutManager(this)
        this.countriesAdapter = CountriesAdapter(this)
        rv_countries.adapter = this.countriesAdapter
    }

    private fun resetEditTextView() {
        et_searchbar.clearFocus()
        et_searchbar.text.clear()
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
            currentFocus?.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    private fun displayToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun displayCountries(countries: List<Country>) {
        countriesAdapter.setData(countries)
    }
}