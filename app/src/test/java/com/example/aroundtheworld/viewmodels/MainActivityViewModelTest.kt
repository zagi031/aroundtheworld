package com.example.aroundtheworld.viewmodels

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.aroundtheworld.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.BeforeEach
import org.junit.rules.TestRule
import org.mockito.Mockito


class MainActivityViewModelTest {

    private lateinit var appMock: Application
    private lateinit var viewmodel: MainActivityViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    val testDispatcher = TestCoroutineDispatcher()

    @Before
    @BeforeEach
    fun setUp() {
        appMock = Mockito.mock(Application::class.java)
        viewmodel = MainActivityViewModel(appMock)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun test_getCountries_For_Scenario_EmptyInput() = runBlockingTest {
        val input = String()
        val expectedToast = MainActivityViewModel.TOASTVALUE_EMPTYSTRING

        viewmodel.getCountries(input)
        val result = viewmodel.toast.getOrAwaitValue()

        assert(expectedToast == result)
    }

    @Test
    fun test_getCountries_For_Scenario_NonExistingCountry() = runBlockingTest {
        val input = "aaaa"
        val expectedToast = MainActivityViewModel.TOASTVALUE_UNMATCHEDCOUNTRYNAME

        viewmodel.getCountries(input)
        val resultToast = viewmodel.toast.getOrAwaitValue()

        assert(expectedToast == resultToast)
    }

    @Test
    fun test_getCountries_For_Scenario_ExistingCountries() = runBlockingTest {
        val input = "cro"
        val expectedNumberOfCountriesToFind = 2


        viewmodel.getCountries(input)
        val result = viewmodel.countries.getOrAwaitValue()

        assert(result.size == expectedNumberOfCountriesToFind)
    }

    @Test
    fun test_getCountries_For_Scenario_AllCountries() = runBlockingTest {
        val input = "all"
        val numberOfCountries = 250

        viewmodel.getCountries(input)
        val result = viewmodel.countries.getOrAwaitValue()

        assert(result.size == numberOfCountries)
    }
}