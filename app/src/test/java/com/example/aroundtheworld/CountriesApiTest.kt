package com.example.aroundtheworld

import com.example.aroundtheworld.viewmodels.MainActivityViewModel
import org.junit.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.stream.Stream

class CountriesApiTest {

    private val retrofit: Retrofit = Retrofit.Builder().baseUrl(MainActivityViewModel.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).build()
    private val countriesApi: CountriesApi = retrofit.create(CountriesApi::class.java)


    companion object {
        @JvmStatic
        fun argumentsForMultipleExistingCountries() = Stream.of(
            Arguments.of("cro", 2),
            Arguments.of("bel", 4),
            Arguments.of("za", 7)
        )
    }

    @Test
    fun test_getCountriesByName_For_Scenario_EmptyInput() {
        val input = String()

        val resultBody = countriesApi.getCountriesByName(input).execute().body()

        assert(resultBody == null)
    }

    @ParameterizedTest
    @ValueSource(strings = ["aaaa", "bbbb"])
    fun test_getCountriesByName_For_Scenario_NonExistingCountryName(input: String) {
        val resultBody = countriesApi.getCountriesByName(input).execute().body()

        assert(resultBody == null)
    }

    @ParameterizedTest
    @ValueSource(strings = ["croatia", "slovenia"])
    fun test_getCountriesByName_For_Scenario_ExistingCountryName(input: String) {
        val resultBody = countriesApi.getCountriesByName(input).execute().body()

        assert(resultBody?.size == 1)
    }

    @ParameterizedTest
    @MethodSource("argumentsForMultipleExistingCountries")
    fun test_getCountriesByName_For_Scenario_MultipleExistingCountryName(input: String, numberOfCountriesToFind: Int) {
        var countryContainsInput = false
        val resultBody = countriesApi.getCountriesByName(input).execute().body()
        resultBody?.forEach {
            it.altSpellings.forEach {
                if(it.contains(input,ignoreCase = true)){
                    countryContainsInput = true
                }
            }
        }
        assert(resultBody?.size == numberOfCountriesToFind)
        assert(countryContainsInput)
    }

    @Test
    fun test_getAllCountries() {
        val numberOfCountries = 250

        val resultBody = countriesApi.getAllCountries().execute().body()

        assert(resultBody?.size == numberOfCountries)
    }
}