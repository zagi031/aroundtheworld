# Around the world

Around the world is an Android application for browsing countries. To get the result, it is necessary to be connected to the internet on your device and type the name of the country (or the substring of the name) inside the search bar and lastly click on the search button. 

## Testing
JUnit was used for unit testing and Espresso was used for UI testing.
Unit tests can be found in the [app/src/test](https://gitlab.com/zagi031/aroundtheworld/-/tree/master/app/src/test/java/com/example/aroundtheworld) while the UI tests can be found in the [app/src/androidTest](https://gitlab.com/zagi031/aroundtheworld/-/tree/master/app/src/androidTest/java/com/example/aroundtheworld).

To run the tests on your PC, you need to: clone this project, have Android Studio installed, open the test classes which can be found in app/java/com.example.aroundtheworld(androidTest) (for UI testing) and app/java/com.example.aroundtheworld(test) (for unit testing) packages in Android Studio. 

For UI testing you need a device to run the tests on, it can be a physical Android mobile device or it can be an Android virtual device (emulator).
For unit testing you don't need any Android device.
To run the tests just open the wanted test class and on the left of the class name, click on the _green run icon_.

### UI tests
Classes: [MainActivityTest](https://gitlab.com/zagi031/aroundtheworld/-/blob/master/app/src/androidTest/java/com/example/aroundtheworld/views/MainActivityTest.kt)

### Unit tests
Classes: [MainActivityViewModelTest](https://gitlab.com/zagi031/aroundtheworld/-/blob/master/app/src/test/java/com/example/aroundtheworld/viewmodels/MainActivityViewModelTest.kt), [CountriesApiTest](https://gitlab.com/zagi031/aroundtheworld/-/blob/master/app/src/test/java/com/example/aroundtheworld/CountriesApiTest.kt)
